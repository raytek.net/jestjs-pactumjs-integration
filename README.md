# JestJS - PactumJS Integration

> Simple integration between JestJS and PactumJS.

# Getting Started

### Prerequisites:
 - NodeJS `v12.22.1`

### How to run?

Inside of the project folder run:

 1. `yarn install`
 1. `yarn ci`

After that you should see a `./output` folder with some `HTML` reports.